﻿using API.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SharedProject.Models;

namespace API.Authentication
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        // Properties, meaning our extra tables in database.
        public DbSet<ApiPartnerModel> Partners { get; set; }
        public DbSet<NewsModel> News { get; set; }
        public DbSet<FeedbackModel> Feedback { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
