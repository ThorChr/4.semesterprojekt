﻿namespace API.Authentication
{
    public class UserRoles
    {
        public const string User = "User";
        public const string Partner = "Partner";
        public const string Admin = "Admin";
    }
}
