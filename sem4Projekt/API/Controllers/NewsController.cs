﻿using API.Authentication;
using API.Models;
using API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SharedProject.Models;
using System.Security.Claims;

namespace API.Controllers
{
    [Authorize(Roles = UserRoles.Partner)]
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;

        public NewsController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }


        /// <summary>
        /// Used for getting all current news articles created.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Get()
        {
            try
            {
                var news = await _db.News.ToListAsync();
                return (news != null) ? Ok(news) : NotFound(new ResponseModel { Status = "Error", Message = "Failed to get news articles." });
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Gets a specific news articles based on the provided Id.
        /// </summary>
        /// <param name="id">Id of the news article of interest.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:int}")]
        [AllowAnonymous]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var news = await _db.News.FirstAsync(x => x.Id == id);
                return (news is not null) ? Ok(news) : NotFound(new ResponseModel { Status = "Error", Message = "News not found."});
            }
            catch
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Gets all news articles from a specific partner.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllFromPartner/{id:int}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllFromPartner(int id)
        {
            try
            {
                // Find the partner.
                var partner = await _db.Partners.Include(x => x.News).FirstAsync(x => x.Id == id);
                if (partner is null)
                    return NotFound();

                // Get all news from the partner.
                var news = partner.News;

                //return (news is not null) ? Ok(news) : BadRequest(new ResponseModel { Status = "Error", Message = "No news foound for the partner."});
                return Ok(news);
            }
            catch
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Takes a post request and finds relevant information from body. Used for adding a new news to the database.
        /// Requires that the user is authorized.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] NewsModel model)
        {
            try
            {
                // Check if we have the needed information.
                if (string.IsNullOrWhiteSpace(model.Title) ||  string.IsNullOrWhiteSpace(model.Description) || string.IsNullOrWhiteSpace(model.Image))
                    return BadRequest(new ResponseModel { Status = "Error", Message = "Missing values for news." });

                var partner = await GetPartnerFromAuthorizedUser();
                if(partner is null)
                    return BadRequest(new ResponseModel { Status = "Error", Message = "No partner found for authorized user." });

                // Add the new news.
                partner.News.Add(model);
                await _db.SaveChangesAsync();

                return Ok();

            } catch
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Updated a specific news article.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] NewsModel model)
        {
            try
            {
                // Get the wanted news article.
                var news = await _db.News.FirstAsync(x => x.Id == model.Id);

                // Update the information.
                news.Title = model.Title;
                news.Description = model.Description;
                news.Image = model.Image;
                news.Link = model.Link;

                // Save changes.
                await _db.SaveChangesAsync();

                return Ok(new ResponseModel { Status = "Success", Message = "News updated."});
            } catch
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Route("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            // Get the specific news thing.
            var news = await _db.News.FindAsync(id);
            if(news is null)
                return NotFound();

            // Remove the stuff.
            _db.News.Remove(news);
            await _db.SaveChangesAsync();

            return Ok(new ResponseModel { Status = "Success", Message = "Item deleted." });
        }

        /// <summary>
        /// Used internally for getting the partner associated to the authorized user.
        /// </summary>
        /// <returns></returns>
        private async Task<ApiPartnerModel> GetPartnerFromAuthorizedUser()
        {
            // Get the partner object, for the user trying to make a new news post.
            ApplicationUser user = await _userManager.FindByNameAsync(
                User.Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault()?.Value);

            // Get the partner based on the user.
            ApiPartnerModel partner = await _db.Partners.Include(x => x.News).FirstAsync(x => x.AssignedUser.Id == user.Id);

            return partner;
        }

    }
}