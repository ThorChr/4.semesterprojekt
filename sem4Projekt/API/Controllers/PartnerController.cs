﻿using API.Authentication;
using API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SharedProject.Models;
using System.Security.Claims;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PartnerController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        public PartnerController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        /// <summary>
        /// Used for getting all current partners in the database.
        /// </summary>
        /// <returns>A collection of partners.</returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                List<ApiPartnerModel> partnersFromDb = await _db.Partners.ToListAsync();

                List<PartnerModel> partners = new List<PartnerModel>();
                foreach (var p in partnersFromDb)
                    partners.Add(new PartnerModel { Id = p.Id, Name = p.Name, Description = p.Description, Logo = p.Logo });

                return (partners != null && partners.Count > 0) ? Ok(partners) : NotFound(new ResponseModel { Status = "Error", Message = "No partner found."});
            } catch (Exception ex)
            {
                return BadRequest(new ResponseModel { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Used for getting a single partner, based on its Id.
        /// Has guard clause for null check for the Id.
        /// </summary>
        /// <param name="id">Id of the partner to be found.</param>
        /// <returns>A PartnerModel object containing information related to that partner.</returns>
        [HttpGet]
        [Route("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var partnerFromDb = await _db.Partners.FindAsync(id);

                if (partnerFromDb == null)
                    return NotFound(new ResponseModel { Status = "Error", Message = "Partner not found." });

                var partner = new PartnerModel { Id = partnerFromDb.Id, Name = partnerFromDb.Name, Description = partnerFromDb.Description, Logo = partnerFromDb.Logo, Latitude = partnerFromDb.Latitude, Longitude = partnerFromDb.Longitude };

                return (partner != null) ? Ok(partner) : NotFound();
            } catch (Exception ex)
            {
                return BadRequest(new ResponseModel { Status = "Error", Message = ex.Message });
            }
        }
        
        [HttpGet]
        [Route("GetAssociatedPartner")]
        public async Task<IActionResult> GetAssociatedPartner()
        {
            try
            {
                // Get partner information based on token.
                var user = await _userManager.FindByNameAsync(
                    User.Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault()?.Value);
                ApiPartnerModel apiPartner = _db.Partners.First(x => x.AssignedUser == user);

                // Convert ApiPartner to (shared) PartnerModel.
                PartnerModel partner = new PartnerModel();
                partner.Id = apiPartner.Id;
                partner.Name = apiPartner.Name;
                partner.Description = apiPartner.Description;
                partner.Logo = apiPartner.Logo;

                // Return model if it exists in the database.
                return (partner != null) ? Ok(partner) : NotFound();
            } catch (Exception ex)
            {
                return BadRequest(new ResponseModel { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Used for creating a partner, and associating said partner to the user being used to create it.
        /// Has guard clauses for empty partner name and description.
        /// Has guard clauses for if user already has a partner associated.
        /// </summary>
        /// <param name="model">PartnerModel object, containing relevant information about the partner being created.</param>
        /// <returns>If it went well, return an OK, with the message "Partner Created".</returns>
        [HttpPost]
        [Authorize(Roles = UserRoles.Partner)]
        public async Task<IActionResult> Post([FromBody] PartnerModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Name) || string.IsNullOrEmpty(model.Description))
                    return BadRequest();

                // Find the user, based on the username provided by the JWT token.
                var user = await _userManager.FindByNameAsync(
                    User.Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault()?.Value);

                // Check if the user already has created a Partner before.
                if (await _db.Partners.AnyAsync(x => x.AssignedUser == user))
                    return BadRequest(new ResponseModel { Status = "Error", Message = "User has already created a partner before." });

                // If we all good to go, then create a new apiPartnerModel so we can add the association to the database.
                ApiPartnerModel apiPartnerModel = new ApiPartnerModel()
                {
                    Name = model.Name,
                    Description = model.Description,
                    AssignedUser = user,
                    Logo = model.Logo
                };

                // Add to database.
                await _db.Partners.AddAsync(apiPartnerModel);
                await _db.SaveChangesAsync();

                return Ok(new ResponseModel { Status = "Success", Message = "Partner created." });
            } catch (Exception ex)
            {
                return BadRequest(new ResponseModel { Status = "Error", Message = ex.Message });
            }
        }

        [HttpDelete]
        [Route("{id:int}")]
        [Authorize(Roles = UserRoles.Partner)]
        public async Task<IActionResult> Delete(int id)
        {
            var partner = await _db.Partners.FindAsync(id);

            if (partner == null)
                return NotFound(new ResponseModel { Status = "Error", Message = "Partner not found."});

            var user = await _userManager.FindByNameAsync(
                User.Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault()?.Value);

            if (user == null || partner.AssignedUser != user)
                return BadRequest(new ResponseModel { Status = "Error", Message = "User not associated to partner." });

            _db.Partners.Remove(partner);
            await _db.SaveChangesAsync();
            return Ok(new ResponseModel { Status = "Success", Message = "Partner deleted." });
        }

        [HttpPut]
        [Route("{id:int}")]
        [Authorize(Roles = UserRoles.Partner)]
        public async Task<IActionResult> Put(int id, [FromBody] PartnerModel model)
        {
            var partner = await _db.Partners.FindAsync(id);

            if (partner == null)
                return NotFound(new ResponseModel { Status = "Error", Message = "Partner not found." });

            var user = await _userManager.FindByNameAsync(
                User.Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault()?.Value);

            if (user == null || partner.AssignedUser != user)
                return BadRequest(new ResponseModel { Status = "Error", Message = "User not associated to partner." });

            if (string.IsNullOrEmpty(partner.Name) || string.IsNullOrEmpty(partner.Description) || string.IsNullOrEmpty(partner.Logo))
                return BadRequest(new ResponseModel { Status = "Error", Message = "Bad request." });

            partner.Name = model.Name;
            partner.Description = model.Description;
            partner.Logo = model.Logo;

            await _db.SaveChangesAsync();
            return Ok(new ResponseModel { Status = "Success", Message = "Partner information updated." });
        }
    }
}
