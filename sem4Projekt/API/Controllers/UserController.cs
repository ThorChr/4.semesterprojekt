﻿using API.Authentication;
using API.Models;
using API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SharedProject.Models;
using SharedProject.Models.Auth;
using System.Security.Claims;

namespace API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        /// <summary>
        /// Endpoint used for getting the partner associated to the currently logged in user.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPartner")]
        public async Task<IActionResult> Partner()
        {
            try
            {
                // Get the currently authorized user.
                ApplicationUser user = await _userManager.FindByNameAsync(
                    User.Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault()?.Value);
                
                if (user == null)
                    return BadRequest();

                // Get the partner associated to the current user.
                var partner = await _db.Partners.FirstAsync(x => x.AssignedUser.Id == user.Id);

                // Convert ApiPartner to Partner.
                PartnerModel result = new()
                {
                    Id = partner.Id,
                    Name = partner.Name,
                    Description = partner.Description,
                    Logo = partner.Logo,
                    Latitude = partner.Latitude,
                    Longitude = partner.Longitude
                };

                return (partner is not null) ? Ok(result) : BadRequest(new ResponseModel { Status = "Error", Message = "User has no partner assigned." });

            } catch (Exception ex)
            {
                return BadRequest(new ResponseModel { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Used for getting the Id of the currently authorized user.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetId")]
        public async Task<IActionResult> GetId()
        {
            try
            {
                // Get the currently authorized user.
                ApplicationUser user = await _userManager.FindByNameAsync(
                    User.Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault()?.Value);

                return (user != null) ? Ok(user.Id) : BadRequest();
            } catch 
            {
                return BadRequest();
            }
        }
        
        [HttpGet]
        [Route("GetDetails")]
        public async Task<IActionResult> GetDetails()
        {
            // Get the currently authorized user.
            ApplicationUser user = await _userManager.FindByNameAsync(
                User.Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault()?.Value);
            
            if (user is null)
            {
                return BadRequest();
            }

            // We found user based on bearer token, then yeet some specific info back.
            return Ok(new UserDetailsModel 
            {
                Id = user.Id,
                Email = user.Email
            });
        }

        // Method for editing a users information.
        // This is done based on the provided id, and the information provided.
        [HttpPut]
        [Route("Edit")]
        public async Task<IActionResult> Edit(EditUserModel model)
        {
            // Get the currently authorized user.
            ApplicationUser user = await _userManager.FindByNameAsync(
                User.Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault()?.Value);

            if (user is null)
            {
                return BadRequest(new ResponseModel { Status = "Error", Message = "User not found." });
            }

            // Check if the provided id is the same as the currently authorized user.
            if (user.Id != model.Id)
            {
                return BadRequest(new ResponseModel { Status = "Error", Message = "User id does not match." });
            }

            // Check if the provided email is the same as the currently authorized user.
            if (user.Email != model.Email)
            {
                // Check if the provided email is already in use.
                if (await _userManager.FindByEmailAsync(model.Email) is not null)
                {
                    return BadRequest(new ResponseModel { Status = "Error", Message = "Email is already in use." });
                }
            }

            // Update the user.
            user.Email = model.Email;

            // Save the changes.
            await _userManager.UpdateAsync(user);

            return Ok();
        }

        private async Task<ApiPartnerModel> GetPartnerFromAuthorizedUser()
        {
            // Get the partner object, for the user trying to make a new news post.
            ApplicationUser user = await _userManager.FindByNameAsync(
                User.Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault()?.Value);

            // Get the partner based on the user.
            ApiPartnerModel partner = await _db.Partners.Include(x => x.News).FirstAsync(x => x.AssignedUser.Id == user.Id);

            return partner;
        }
    }
}
