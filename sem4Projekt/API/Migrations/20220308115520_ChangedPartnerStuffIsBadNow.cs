﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace API.Migrations
{
    public partial class ChangedPartnerStuffIsBadNow : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AssignedUserId",
                table: "Partners",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Partners_AssignedUserId",
                table: "Partners",
                column: "AssignedUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Partners_AspNetUsers_AssignedUserId",
                table: "Partners",
                column: "AssignedUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Partners_AspNetUsers_AssignedUserId",
                table: "Partners");

            migrationBuilder.DropIndex(
                name: "IX_Partners_AssignedUserId",
                table: "Partners");

            migrationBuilder.DropColumn(
                name: "AssignedUserId",
                table: "Partners");
        }
    }
}
