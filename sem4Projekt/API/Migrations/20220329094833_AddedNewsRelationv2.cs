﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace API.Migrations
{
    public partial class AddedNewsRelationv2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ApiPartnerModelId",
                table: "News",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_News_ApiPartnerModelId",
                table: "News",
                column: "ApiPartnerModelId");

            migrationBuilder.AddForeignKey(
                name: "FK_News_Partners_ApiPartnerModelId",
                table: "News",
                column: "ApiPartnerModelId",
                principalTable: "Partners",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_News_Partners_ApiPartnerModelId",
                table: "News");

            migrationBuilder.DropIndex(
                name: "IX_News_ApiPartnerModelId",
                table: "News");

            migrationBuilder.DropColumn(
                name: "ApiPartnerModelId",
                table: "News");
        }
    }
}
