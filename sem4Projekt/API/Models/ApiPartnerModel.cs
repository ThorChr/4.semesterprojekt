﻿using API.Authentication;
using SharedProject.Models;

namespace API.Models
{
    public class ApiPartnerModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Logo { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public ApplicationUser AssignedUser { get; set; }
        public List<NewsModel> News { get; set; }
    }
}
