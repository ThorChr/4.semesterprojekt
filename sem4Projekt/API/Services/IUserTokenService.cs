﻿using API.Authentication;
using API.Models;

namespace API.Services
{
    public interface IUserTokenService
    {
        Task<ApplicationUser?> GetCurrentUser();
    }
}
