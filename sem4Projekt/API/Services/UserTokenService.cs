﻿using API.Authentication;
using API.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace API.Services
{
    // Eksde, this entire thing doesn't work...
    public class UserTokenService : ControllerBase, IUserTokenService
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        public UserTokenService(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }

        /// <summary>
        /// Used for getting information about the currently authorized user based on the provided JWT.
        /// </summary>
        /// <returns></returns>
        public async Task<ApplicationUser?> GetCurrentUser()
        {
            // Get partner information based on token.
            var user = await _userManager.FindByNameAsync(User.Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault()?.Value);

            if(user == null)
                return null;

            return user;
        }
    }
}
