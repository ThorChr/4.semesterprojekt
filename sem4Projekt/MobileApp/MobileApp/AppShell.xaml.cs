﻿using MobileApp.Views;
using MobileApp.Views.Authentication;
using MobileApp.Views.SettingsPages;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MobileApp
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(PartnerDetailPage), typeof(PartnerDetailPage));
            Routing.RegisterRoute(nameof(NewsDetailPage), typeof(NewsDetailPage));

            // Routes for authentication pages.
            Routing.RegisterRoute(nameof(LoginPage), typeof(LoginPage));
            Routing.RegisterRoute(nameof(RegisterPage), typeof(RegisterPage));
            Routing.RegisterRoute(nameof(LogoutPage), typeof(LogoutPage));

            // Register routes for the settings pages.
            Routing.RegisterRoute(nameof(FeedbackPage), typeof(FeedbackPage));
            Routing.RegisterRoute(nameof(AboutUsSettingsPage), typeof(AboutUsSettingsPage));
            Routing.RegisterRoute(nameof(ContactUsSettingsPage), typeof(ContactUsSettingsPage));
            Routing.RegisterRoute(nameof(PrivacyPolicyPage), typeof(PrivacyPolicyPage));
            Routing.RegisterRoute(nameof(TermsConditionsSettingsPage), typeof(TermsConditionsSettingsPage));
            Routing.RegisterRoute(nameof(MyAccountPage), typeof(MyAccountPage));
        }

    }
}
