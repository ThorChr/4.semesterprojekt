﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace MobileApp.Helpers
{
    /// <summary>
    /// Helpers class for calling the API.
    /// </summary>
    public class CallApi
    {
        public static HttpClient _client { get; private set; } = new HttpClient(new HttpClientHandler()
        {
            ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; }
        });

        /// <summary>
        /// Used for posting to the API.
        /// </summary>
        /// <param name="uri">Full path we're posting to.</param>
        /// <param name="stringContent">Content we are sending, in the form of StringContent.</param>
        /// <returns>A task of HttpResponseMessage is returned.</returns>
        public async static Task<HttpResponseMessage> PostAsync(Uri uri, StringContent stringContent, bool useBearerToken = false)
        {
            HttpClient tempClient = (useBearerToken) ? GetHttpClientWithAuth() : _client;

            var request = new HttpRequestMessage(HttpMethod.Post, uri);
            request.Content = stringContent;

            var response = await _client.SendAsync(request);

            return response;
        }

        /// <summary>
        /// Puts information to the given URI.
        /// </summary>
        /// <param name="uri">URI to access.</param>
        /// <param name="stringContent">Content to put.</param>
        /// <param name="useBearerToken">Specifies if we're going to use the bearer token for the logged in user or not.</param>
        /// <returns></returns>
        public async static Task<HttpResponseMessage> PutAsync(Uri uri, StringContent stringContent, bool useBearerToken = false)
        {
            HttpClient tempClient = (useBearerToken) ? GetHttpClientWithAuth() : _client;

            var request = new HttpRequestMessage(HttpMethod.Put, uri);
            request.Content = stringContent;

            var response = await _client.SendAsync(request);

            return response;
        }

        /// <summary>
        /// Gets information from the given URI.
        /// </summary>
        /// <param name="uri">URI to access.</param>
        /// <param name="useBearerToken">Specifies if we're going to use bearer token or no.</param>
        /// <returns></returns>
        public async static Task<HttpResponseMessage> GetAsync(Uri uri, bool useBearerToken = false)
        {
            HttpClient tempClient = (useBearerToken) ? GetHttpClientWithAuth() : _client;
            
            var response = await tempClient.GetAsync(uri);
            
            return response;
        }

        private static HttpClient GetHttpClientWithAuth()
        {
            var client = _client;
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Preferences.Get("Token", ""));

            return client;
        }

    }
}
