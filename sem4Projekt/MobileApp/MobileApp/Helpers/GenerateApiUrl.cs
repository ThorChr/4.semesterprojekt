﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace MobileApp.Helpers
{
    public class GenerateApiUrl
    {
        /// <summary>
        /// Used for getting the link to the API.
        /// </summary>
        /// <param name="relativeUrl">The base address is supplied in this method, it just needs the relative path, meaning the /api/News/2 stuff.</param>
        /// <returns></returns>
        public static Uri Get(string relativeUrl)
        {
            // Create URL.
            string baseAddress = DeviceInfo.Platform == DevicePlatform.Android ? "https://10.0.2.2:7257" : "https://localhost:7257";
            var uri = new Uri(String.Format(baseAddress + relativeUrl, string.Empty));

            return uri;
        }

    }
}
