﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileApp.Models
{
    public class SettingsItem
    {
        public string Title { get; set; }
        public string NameOf { get; set; }
        public string Icon { get; set; }
    }
}
