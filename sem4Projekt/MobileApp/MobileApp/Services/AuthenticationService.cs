﻿using SharedProject.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MobileApp.Helpers;
using System.Net.Http;
using Newtonsoft.Json;
using Xamarin.Essentials;
using SharedProject.Models;

namespace MobileApp.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        /// <summary>
        /// Used for logging the user in.
        /// </summary>
        /// <param name="loginRequest">Class contaning the email and password the user used for trying to login.</param>
        /// <returns>True / False depending if success or no.</returns>
        public async Task<AuthenticationResult> Login(UserLoginRequest loginRequest)
        {
            var uri = GenerateApiUrl.Get("/api/Authentication/User/Login");
            var content = new StringContent(JsonConvert.SerializeObject(loginRequest), Encoding.UTF8, "application/json");
            var response = await CallApi.PostAsync(uri, content);

            if (response.IsSuccessStatusCode)
            {
                // Deserialize result.
                var result = JsonConvert.DeserializeObject<AuthenticationResult>(await response.Content.ReadAsStringAsync());

                // Store token to preferences.
                Preferences.Set("Token", result.Token);

                return new AuthenticationResult
                {
                    Success = true,
                    Token = result.Token
                };
            } else
            {
                var result = JsonConvert.DeserializeObject<ResponseModel>(await response.Content.ReadAsStringAsync());
                return new AuthenticationResult()
                {
                    Success = false,
                    Errors = result.Message
                };
            }
        }

        /// <summary>
        /// Logs the user out.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Logout()
        {
            try
            {
                // Remove token from preferences.
                Preferences.Remove("Token");
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Used for registering a new user.
        /// </summary>
        /// <param name="registerRequest">Registration information provided by the user.</param>
        /// <returns>True / False depending on success or no.</returns>
        public async Task<AuthenticationResult> Register(UserRegisterRequest registerRequest)
        {
            var uri = GenerateApiUrl.Get("/api/Authentication/User/Register");
            var content = new StringContent(JsonConvert.SerializeObject(registerRequest), Encoding.UTF8, "application/json");
            var response = await CallApi.PostAsync(uri, content);

            var result = JsonConvert.DeserializeObject<ResponseModel>(await response.Content.ReadAsStringAsync());

            if (!response.IsSuccessStatusCode)
            {
                return new AuthenticationResult()
                {
                    Success = false,
                    Errors = result.Message
                };
            }

            return new AuthenticationResult()
            {
                Success = true
            };
        }
    }
}
