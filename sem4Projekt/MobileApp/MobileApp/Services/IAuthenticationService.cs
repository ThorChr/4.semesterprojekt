﻿using SharedProject.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MobileApp.Services
{
    public interface IAuthenticationService
    {
        Task<AuthenticationResult> Login(UserLoginRequest loginRequest);
        Task<AuthenticationResult> Register(UserRegisterRequest registerRequest);
        Task<bool> Logout();
    }
}
