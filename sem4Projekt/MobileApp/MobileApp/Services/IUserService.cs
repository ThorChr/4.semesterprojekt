﻿using SharedProject.Contracts.Edit;
using SharedProject.Models.Auth;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MobileApp.Services
{
    public interface IUserService
    {
        Task<UserDetailsModel> GetUserDetails();
        Task<EditUserResult> SaveUserDetails(UserDetailsModel model);
    }
}
