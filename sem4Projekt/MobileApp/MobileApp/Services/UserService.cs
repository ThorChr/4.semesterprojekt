﻿using MobileApp.Helpers;
using Newtonsoft.Json;
using SharedProject.Contracts.Edit;
using SharedProject.Models.Auth;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MobileApp.Services
{
    public class UserService : IUserService
    {
        public async Task<UserDetailsModel> GetUserDetails()
        {
            var uri = GenerateApiUrl.Get("/api/User/GetDetails");
            var response = await CallApi.GetAsync(uri, useBearerToken: true);

            if (!response.IsSuccessStatusCode) 
            {
                return null;
            }

            return JsonConvert.DeserializeObject<UserDetailsModel>(await response.Content.ReadAsStringAsync());
        }
        
        public async Task<EditUserResult> SaveUserDetails(UserDetailsModel model)
        {
            var uri = GenerateApiUrl.Get("/api/User/Edit");
            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            var response = await CallApi.PutAsync(uri, content, useBearerToken: true);

            if(!response.IsSuccessStatusCode)
            {
                var result = JsonConvert.DeserializeObject<EditUserResult>(await response.Content.ReadAsStringAsync());
                result.Success = false;
                return result;
            }

            return new EditUserResult 
            { 
                Success = true,
                Message = "User details updated successfully",
                EditUser = await GetUserDetails()
            };
        }
    }
}
