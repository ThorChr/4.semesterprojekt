﻿using MobileApp.Services;
using MobileApp.Views;
using MobileApp.Views.Authentication;
using MvvmHelpers.Commands;
using SharedProject.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace MobileApp.ViewModels.Authentication
{
    public class LoginViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authService;

        // Commands.
        public AsyncCommand CheckIfTokenExists { get; set; }
        public AsyncCommand LoginButtonPressed { get; set; }
        public AsyncCommand RegisterButtonPressed { get; private set; }


        // Properties.
        public UserLoginRequest LoginRequest { get; set; }

        public LoginViewModel()
        {
            Title = "Login";

            // Dependency injection.
            _authService = DependencyService.Get<IAuthenticationService>();

            // Command handling.
            CheckIfTokenExists = new AsyncCommand(CheckIfTokenExistsAsync);
            LoginButtonPressed = new AsyncCommand(LoginButtonPressedAsync);

            RegisterButtonPressed = new AsyncCommand(NavigateToRegister);


            // Initialize properties.
            LoginRequest = new UserLoginRequest();
        }

        private async Task LoginButtonPressedAsync()
        {
            if(LoginRequest is null)
            {
                return;
            }

            if(string.IsNullOrWhiteSpace(LoginRequest.Email) || string.IsNullOrWhiteSpace(LoginRequest.Password))
            {
                return;
            }

            // Try to log the user in.
            try
            {
                var result = await _authService.Login(LoginRequest);
                
                // If we failed to log in.
                if(!result.Success)
                {
                    await Application.Current.MainPage.DisplayAlert("Login Failed", "Invalid email or password.", "OK");
                    LoginRequest.Password = string.Empty;
                    OnPropertyChanged("LoginRequest");
                    return;
                }

                // If we logged in successfully.
                LoginRequest = new UserLoginRequest();
                OnPropertyChanged("LoginRequest");
                await Shell.Current.GoToAsync($"//{nameof(AboutPage)}");
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Login Error", ex.Message, "OK");
            }
        }

        /// <summary>
        /// If the token exists in the preferences i.e. the user is logged in, yeet them to the home page.
        /// </summary>
        /// <returns></returns>
        private async Task CheckIfTokenExistsAsync()
        {
            var token = Preferences.Get("Token", string.Empty);
            if (string.IsNullOrEmpty(token))
            {
                return;
            }

            await Shell.Current.GoToAsync($"//{nameof(AboutPage)}");
        }

        private async Task NavigateToRegister() => await Shell.Current.GoToAsync($"{nameof(RegisterPage)}");
    }
}
