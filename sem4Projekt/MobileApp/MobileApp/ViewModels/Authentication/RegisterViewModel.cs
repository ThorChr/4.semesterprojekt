﻿using MobileApp.Services;
using MobileApp.Views.Authentication;
using MvvmHelpers.Commands;
using SharedProject.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobileApp.ViewModels.Authentication
{
    public class RegisterViewModel : ViewModelBase
    {
        private readonly IAuthenticationService _authService;

        public AsyncCommand RegisterButtonPressed { get; private set; }

        private UserRegisterRequest _registerRequest;
        public UserRegisterRequest RegisterRequest
        {
            get => _registerRequest;
            set
            {
                _registerRequest = value;
                OnPropertyChanged();
            }
        }


        public RegisterViewModel()
        {
            Title = "Register";
            _authService = DependencyService.Get<IAuthenticationService>();
            RegisterButtonPressed = new AsyncCommand(RegisterAsync);
            RegisterRequest = new UserRegisterRequest();

        }

        private async Task RegisterAsync()
        {
            if (RegisterRequest is null)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Please fill in the necessecary data.", "Ok");
                return;
            }

            if (string.IsNullOrEmpty(RegisterRequest.Email) || string.IsNullOrEmpty(RegisterRequest.Password) || string.IsNullOrEmpty(RegisterRequest.PasswordVerify))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Please fill in the necessecary data.", "Ok");
                return;
            }

            if (RegisterRequest.Password != RegisterRequest.PasswordVerify)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Passwords doesn't match up.", "Ok");
                return;
            }

            // Call auth service and register.
            var result = await _authService.Register(RegisterRequest);

            // Check if result is false, if it is display error.
            if (!result.Success)
            {
                await Application.Current.MainPage.DisplayAlert("Error", $"Error occured while registering. Try again. \n {result.Errors}", "Ok");
                return;
            }

            // Yeet the user over to the login page.
            await Shell.Current.GoToAsync($"{nameof(LoginPage)}");

        }
    }
}
