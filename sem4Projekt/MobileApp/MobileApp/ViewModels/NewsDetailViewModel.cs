﻿using MvvmHelpers.Commands;
using Newtonsoft.Json;
using SharedProject;
using SharedProject.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace MobileApp.ViewModels
{

    [QueryProperty(nameof(NewsId), nameof(NewsId))]
    public class NewsDetailViewModel : ViewModelBase
    {
        private readonly HttpClient _client;

        private int _newsId;
        public int NewsId
        {
            get => _newsId;
            set { 
                _newsId = value;
                OnPropertyChanged();
                GetNews.ExecuteAsync();
            }
        }

        public AsyncCommand GetNews { get; set; }

        public NewsModel News { get; set; }

        public NewsDetailViewModel()
        {
            GetNews = new AsyncCommand(GetNewsAsync);
            _client = HttpClientConstant.GetClient();
        }

        private async Task GetNewsAsync()
        {
            IsBusy = true;

            // Generate the URI for our API endpoint, and make a Get request to it.
            string baseAddress = DeviceInfo.Platform == DevicePlatform.Android ? "https://10.0.2.2:7257" : "https://localhost:7257";
            var uri = new Uri(String.Format(baseAddress + $"/api/News/{_newsId}", string.Empty));

            // Call API.
            HttpResponseMessage response = await _client.GetAsync(uri);
            if(response.IsSuccessStatusCode)
            {
                // Read the response body.
                var responseBody = await response.Content.ReadAsStringAsync();
                News = JsonConvert.DeserializeObject<NewsModel>(responseBody);
                OnPropertyChanged("News");
            } else
            {
                var msg = "some error";
            }


            IsBusy = false;
        }

    }
}
