﻿using MobileApp.Views;
using MvvmHelpers;
using MvvmHelpers.Commands;
using Newtonsoft.Json;
using SharedProject;
using SharedProject.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace MobileApp.ViewModels
{
    public class NewsViewModel : ViewModelBase
    {
        private readonly HttpClient _client;

        // Don't work due to the way tap gesture recognizer is setup.
        private NewsModel _selectedItem;        
        public NewsModel SelectedItem
        {
            get => _selectedItem;
            set => SetProperty(ref _selectedItem, value);
        }


        public ObservableRangeCollection<NewsModel> NewsCollection { get; set; }

        public AsyncCommand RefreshCommand { get; set; }
        public AsyncCommand<NewsModel> ItemTappedCommand { get; set; }

        public NewsViewModel()
        {
            _client = HttpClientConstant.GetClient();
            NewsCollection = new ObservableRangeCollection<NewsModel>();
            RefreshCommand = new AsyncCommand(RefreshNews);
            ItemTappedCommand = new AsyncCommand<NewsModel>(ItemTapped);

            // Get the news from the API.
            RefreshNews();
        }

        /// <summary>
        /// Upon click of a news item, open the news item in another xaml view page.
        /// </summary>
        /// <returns></returns>
        private async Task ItemTapped(NewsModel news)
        {
            try
            {
                if (Uri.TryCreate(news.Link, UriKind.Absolute, out Uri uri))
                {
                    await Browser.OpenAsync(uri, BrowserLaunchMode.SystemPreferred);
                } else
                {
                    await Application.Current.MainPage.DisplayAlert($"{news.Title} informationer", news.Description, "OK");
                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Error", ex.Message, "OK");
            }
        }

        /// <summary>
        /// Called by the user refreshing the collectionview.
        /// Calls the getnews method, which gets the news from the API.
        /// </summary>
        /// <returns></returns>
        private async Task RefreshNews()
        {
            IsBusy = true;

            await GetNews();

            IsBusy = false;
        }

        /// <summary>
        /// Gets the news from the API.
        /// </summary>
        /// <returns></returns>
        private async Task GetNews()
        {
            // Generate URI.
            string baseAddress = DeviceInfo.Platform == DevicePlatform.Android ? "https://10.0.2.2:7257" : "https://localhost:7257";
            string uri = $"{baseAddress}/api/News";

            try
            {
                // Handle API calling etc.
                HttpResponseMessage response = await _client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    string jsonContent = await response.Content.ReadAsStringAsync();
                    NewsCollection.ReplaceRange(JsonConvert.DeserializeObject<List<NewsModel>>(jsonContent));
                }
                else
                {
                    NewsCollection.Add(new NewsModel { Title = "Error", Description = "Error" });
                }

                OnPropertyChanged("NewsCollection");
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

        }
    }
}