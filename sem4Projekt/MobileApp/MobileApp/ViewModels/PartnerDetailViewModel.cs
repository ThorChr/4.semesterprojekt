﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MvvmHelpers;
using MvvmHelpers.Commands;
using SharedProject.Models;
using System.Net.Http;
using SharedProject;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Essentials;
using MobileApp.Views;

namespace MobileApp.ViewModels
{
    [QueryProperty(nameof(PartnerId), nameof(PartnerId))]
    public class PartnerDetailViewModel : ViewModelBase
    {
        public ObservableRangeCollection<NewsModel> News { get; set; }

        private string _partnerId;
        public string PartnerId
        {
            get => _partnerId;
            set
            {
                _partnerId = value;
                OnPropertyChanged();
                GetPartnerDataAsyncCommand.ExecuteAsync();
                GetPartnerNewsAsyncCommand.ExecuteAsync();
            }
        }

        // Partner of interesst.
        public PartnerModel Partner { get; set; }

        // Commands.
        public AsyncCommand GetPartnerDataAsyncCommand { get; set; }
        public AsyncCommand NavigateButtonAsyncCommand { get; set; }
        public AsyncCommand GetPartnerNewsAsyncCommand { get; set; }
        public AsyncCommand<NewsModel> NewsTappedAsyncCommand { get; set; }

        // Our http client.
        private readonly HttpClient _client;

        public PartnerDetailViewModel()
        {
            // Filling in dummy data for now. Will be replaced with proper data at a later point.
            News = new ObservableRangeCollection<NewsModel>();
            //News.Add(new NewsModel { Id = 1, Title = "Placeholder Nyhed." });
            //News.Add(new NewsModel { Id = 2, Title = "Placeholder Nyhed." });
            //News.Add(new NewsModel { Id = 3, Title = "Placeholder Nyhed." });
            //News.Add(new NewsModel { Id = 4, Title = "Placeholder Nyhed." });
            //News.Add(new NewsModel { Id = 5, Title = "Placeholder Nyhed." });
            //News.Add(new NewsModel { Id = 6, Title = "Placeholder Nyhed." });
            //OnPropertyChanged("News");

            _client = HttpClientConstant.GetClient();

            // Bind needed commands.
            GetPartnerDataAsyncCommand = new AsyncCommand(GetPartnerDataAsync);
            NavigateButtonAsyncCommand = new AsyncCommand(NavigateButtonAsync);
            GetPartnerNewsAsyncCommand = new AsyncCommand(RetrieveNews);
            NewsTappedAsyncCommand = new AsyncCommand<NewsModel>(NewsTappedAsync);
        }

        async Task GetPartnerDataAsync()
        {
            IsBusy = true;

            // Generate URI for API engpoint. This matters, because iPhone and Android emulators handle this stuff differently.
            string baseAddress = DeviceInfo.Platform == DevicePlatform.Android ? "https://10.0.2.2:7257" : "https://localhost:7257";
            var uri = new Uri(string.Format(baseAddress + "/api/Partner/" + _partnerId, string.Empty));

            // Call API.
            HttpResponseMessage response = await _client.GetAsync(uri); 
            if(response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                Partner = JsonConvert.DeserializeObject<PartnerModel>(jsonContent);
            } else
            {
                Partner = new PartnerModel
                {
                    Id = (int)response.StatusCode,
                    Name = "testtesttest",
                    Description = "TESATETETASD SADTERWAS DSATES TEST TEST TEST",
                    Logo = "https://avatars.cloudflare.steamstatic.com/5f18395d412f524e2dc02d57f1c3ba47f367f08b_full.jpg"
                };
            }

            OnPropertyChanged("Partner");

            IsBusy = false;
        }

        async Task NavigateButtonAsync()
        {
            await Application.Current.MainPage.DisplayAlert("NOT YET IMPLEMENTED", "Du trykkede på knappen for at navigere til den nærmeste lokalitet for denne partner. Den funktionalitet er ikke implementeret endnu. Partner navn: " + Partner.Name, "OK");
        }

        async Task RetrieveNews()
        {
            IsBusy = true;

            // Generate URI.
            string baseAddress = DeviceInfo.Platform == DevicePlatform.Android ? "https://10.0.2.2:7257" : "https://localhost:7257";
            string uri = $"{baseAddress}/api/News/GetAllFromPartner/{_partnerId}";

            try
            {
                // Handle API calling etc.
                HttpResponseMessage response = await _client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    string jsonContent = await response.Content.ReadAsStringAsync();
                    News.ReplaceRange(JsonConvert.DeserializeObject<List<NewsModel>>(jsonContent));
                }
                else
                {
                    News.Add(new NewsModel { Title = "Error", Description = "Error" });
                }

                OnPropertyChanged("News");
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

            IsBusy = false;
        }

        async Task NewsTappedAsync(NewsModel news)
        {
            try
            {
                if (Uri.TryCreate(news.Link, UriKind.Absolute, out Uri uri))
                {
                    await Browser.OpenAsync(uri, BrowserLaunchMode.SystemPreferred);
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert($"{news.Title} informationer", news.Description, "OK");
                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Error", ex.Message, "OK");
            }
        }

    }
}