﻿using MobileApp.Views;
using MvvmHelpers;
using MvvmHelpers.Commands;
using SharedProject.Models;
using SharedProject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Essentials;
using Newtonsoft.Json;

namespace MobileApp.ViewModels
{
    public class PartnerViewModel : ViewModelBase
    {
        // Fields.
        private readonly HttpClient _client;

        // Properties.
        public ObservableRangeCollection<PartnerModel> Partners { get; set; }
        private ObservableRangeCollection<PartnerModel> _oldPartners { get; set; }
        private string partnerSearchQueue;
        public string PartnerSearchQueue
        {
            get { return partnerSearchQueue; }
            set 
            { 
                partnerSearchQueue = value;
                SearchCommand.ExecuteAsync(); 
            }
        }

        // Commands.
        public AsyncCommand RefreshCommand { get; }
        public AsyncCommand SearchCommand { get; set; }
        public AsyncCommand<PartnerModel> NavigateCommand { get; set; }
        public Xamarin.Forms.Command<PartnerModel> ItemTapped { get; }

        public PartnerViewModel()
        {
            // Initialize our collections.
            Partners = new ObservableRangeCollection<PartnerModel>();
            _oldPartners = new ObservableRangeCollection<PartnerModel>();

            // Initialize the commands.
            RefreshCommand = new AsyncCommand(RefreshMethod);
            SearchCommand = new AsyncCommand(SearchMethod);
            NavigateCommand = new AsyncCommand<PartnerModel>(NavigateMethod);
            ItemTapped = new Xamarin.Forms.Command<PartnerModel>(TapMethod);

            // Initialize http client.
            _client = HttpClientConstant.GetClient();

            RetrieveData();
        }

        /// <summary>
        /// Navigates the user to a new page in the app.
        /// Sends a parameter to the new page.
        /// Is called upon clicking the partners click area.
        /// </summary>
        /// <param name="partner"></param>
        async void TapMethod(PartnerModel partner)
        {
            var uri = $"{nameof(PartnerDetailPage)}?{nameof(PartnerDetailViewModel.PartnerId)}={partner.Id}";
            await Shell.Current.GoToAsync(uri);
        }

        /// <summary>
        /// Makes a call to the API via our RetrieveData() method.
        /// </summary>
        /// <returns></returns>
        async Task RefreshMethod()
        {
            // We can always add further logic in here, but for now, just call the retrieve data method.
            IsBusy = true;

            await RetrieveData();

            IsBusy = false;
        }

        /// <summary>
        /// Searches through the Partner property based on whatever the user has specified.
        /// </summary>
        /// <returns></returns>
        async Task SearchMethod()
        {
            if (partnerSearchQueue == "" || partnerSearchQueue == null || partnerSearchQueue == " ")
            {
                // Also stupid hack for heap vs stack. Should use ref keyword.
                Partners.Clear();
                foreach(var partner in _oldPartners)
                    Partners.Add(partner);

                OnPropertyChanged("Partners");
                return;
            }

            // Find what user is searching for.
            var localSearchQueue = partnerSearchQueue.Trim().ToLower();
            var newCollection = Partners.Where(x => x.Name.ToLower().Contains(localSearchQueue)).ToList();

            // Update the list.
            Partners.Clear();
            Partners.AddRange(newCollection);
            OnPropertyChanged("Partners");

        }

        /// <summary>
        /// NOT IMPLEMENTED YET.
        /// The plan is the user can click on "navigate" and this will open up the users Map app, and search for the address the Partner has specified.
        /// </summary>
        /// <param name="partner">The partner being clicked on.</param>
        /// <returns></returns>
        async Task NavigateMethod(PartnerModel partner)
        {
            await Application.Current.MainPage.DisplayAlert("Navigation", partner.Name, "OK");
        }

        /// <summary>
        /// Retrieves data from our API, and json convert it to needed model.
        /// Then saves this in Partner property and calls a OnpropertyChanged().
        /// </summary>
        private async Task RetrieveData()
        {
            IsBusy = true;

            // Generate the URI for our API endpoint, and make a Get request to it.
            string baseAddress = DeviceInfo.Platform == DevicePlatform.Android ? "https://10.0.2.2:7257" : "https://localhost:7257";
            var uri = new Uri(String.Format(baseAddress + "/api/Partner/", string.Empty));

            HttpResponseMessage response = await _client.GetAsync(uri);

            // Check the response.
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Partners = JsonConvert.DeserializeObject<ObservableRangeCollection<PartnerModel>>(content);
            }
            else
            {
                Partners = new ObservableRangeCollection<PartnerModel>();
            }

            OnPropertyChanged("Partners");

            // Stupid hack for fixing heap vs stack thing thing.
            // Could probably be fixed by using ref keyword I think.
            foreach (var partner in Partners)
                _oldPartners.Add(partner);

            IsBusy = false;

            #region test data for partners
            //Partners = new ObservableRangeCollection<PartnerModel>() 
            //{
            //    new PartnerModel()
            //    {
            //        Id = 1,
            //        Name = "Lissner Inc.",
            //        Description = "Lorem ipsum dollar sit amet",
            //        Logo = "https://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/31/31b5d455e1df765239ae78f631df15fa5ab874b8_full.jpg",
            //        Latitude = 55.2695119,
            //        Longitude = 9.057263
            //    },
            //    new PartnerModel()
            //    {
            //        Id = 2,
            //        Name = "Freddy Gay.",
            //        Description = "Lorem ipsum dollar sit amet",
            //        Logo = "https://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/b2/b2746c39ddde813c42cf758c7d355bb684b1cc46_full.jpg",
            //        Latitude = 55.2681563,
            //        Longitude = 9.8936206
            //    },
            //    new PartnerModel()
            //    {
            //        Id = 3,
            //        Name = "Who Tao?",
            //        Description = "Lorem ipsum dollar sit amet",
            //        Logo = "https://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/f3/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg",
            //        Latitude = 55.2295773,
            //        Longitude = 9.9595976
            //    },
            //    new PartnerModel()
            //    {
            //        Id = 4,
            //        Name = "The Line",
            //        Description = "ARGHHHHH",
            //        Logo = "https://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/3c/3c95c352f6028a970c84b0b4e81ee3b6f2c22240_full.jpg",
            //        Latitude = 55.2681563,
            //        Longitude = 9.8936206
            //    }
            //};
            #endregion
        }

    }
}