﻿using MvvmHelpers;
using SharedProject.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileApp.ViewModels
{
    public class RewardsViewModel : ViewModelBase
    {
        public ObservableRangeCollection<RewardsModel> RewardsCollection { get; set; }

        public RewardsViewModel()
        {
            Title = "Gevinster";

            RewardsCollection = GetData();
            OnPropertyChanged("RewardsCollection");
        }
        
        private ObservableRangeCollection<RewardsModel> GetData()
        {
            var collection = new ObservableRangeCollection<RewardsModel>();

            collection.Add(new RewardsModel 
            {
                Title = "25% - Næste køb",
                Image = "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg",
                PartnerName = "LEGO A/S",
                ExpirationDate = DateTime.Now.AddDays(3)
            });

            collection.Add(new RewardsModel
            {
                Title = "1 Gratis Cheeseburger ved dit næste køb.",
                Image = "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg",
                PartnerName = "McDonalds",
                ExpirationDate = DateTime.Now.AddDays(1)
            });

            collection.Add(new RewardsModel
            {
                Title = "Hent én gratis T-shirt med pulver",
                Image = "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg",
                PartnerName = "Fipros A/S",
                ExpirationDate = DateTime.Now.AddDays(3)
            });

            collection.Add(new RewardsModel
            {
                Title = "20% af på dit abonnoment af ordbogen.",
                Image = "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg",
                PartnerName = "Ordbogen APS",
                ExpirationDate = DateTime.Now.AddDays(4)
            });

            collection.Add(new RewardsModel
            {
                Title = "Få 3, betal for 2.",
                Image = "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg",
                PartnerName = "Colorbox APS",
                ExpirationDate = DateTime.Now.AddDays(7)
            });

            return collection;
        }
    }
}
