﻿using MobileApp.Models;
using MobileApp.Views.Authentication;
using MobileApp.Views.SettingsPages;
using MvvmHelpers;
using MvvmHelpers.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobileApp.ViewModels
{
    public class SettingsViewModel : ViewModelBase
    {
        /// <summary>
        /// Contains all the settings items the user can see.
        /// </summary>
        public ObservableRangeCollection<SettingsItem> Pages { get; set; }

        /// <summary>
        /// Gets called when a settings item is tapped.
        /// Used for navigating to the correct page.
        /// </summary>
        public AsyncCommand<SettingsItem> SettingsItemTappedCommand { get; set; }

        public SettingsViewModel()
        {
            Title = "Indstillinger";

            SettingsItemTappedCommand = new AsyncCommand<SettingsItem>(SettingsItemTappedAsync);

            Pages = new ObservableRangeCollection<SettingsItem>
            {
                new SettingsItem
                {
                    Title = "Feedback",
                    NameOf = nameof(FeedbackPage),
                    Icon = "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg"
                },
                new SettingsItem
                {
                    Title = "Min Konto",
                    NameOf = nameof(MyAccountPage),
                    Icon = "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg"
                },
                new SettingsItem
                {
                    Title = "Vilkår og Betingelser",
                    NameOf = nameof(TermsConditionsSettingsPage),
                    Icon = "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg"
                },
                new SettingsItem
                {
                    Title = "Persondatapolitik",
                    NameOf = nameof(PrivacyPolicyPage),
                    Icon = "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg"
                },
                new SettingsItem
                {
                    Title = "Kontakt",
                    NameOf = nameof(ContactUsSettingsPage),
                    Icon = "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg"
                },
                new SettingsItem
                {
                    Title = "Om Os",
                    NameOf = nameof(AboutUsSettingsPage),
                    Icon = "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg"
                },
                new SettingsItem
                {
                    Title = "Log Ud",
                    NameOf = nameof(LogoutPage),
                    Icon = "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg"
                }
            };
        }

        private async Task SettingsItemTappedAsync(SettingsItem item)
        {
            //await Application.Current.MainPage.DisplayAlert("Tapped", item.Title, "OK");
            await Shell.Current.GoToAsync($"{item.NameOf}");
        }
    }
}
