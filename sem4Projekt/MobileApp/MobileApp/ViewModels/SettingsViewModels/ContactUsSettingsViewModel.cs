﻿using MvvmHelpers.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace MobileApp.ViewModels.SettingsViewModels
{
    public class ContactUsSettingsViewModel : ViewModelBase
    {
        public AsyncCommand MailButtonPressed { get; set; }
        
        public ContactUsSettingsViewModel()
        {
            Title = "Kontakt Os";

            MailButtonPressed = new AsyncCommand(NavigateToMailClient);
        }

        private async Task NavigateToMailClient()
        {
            var address = "voresemail@voresdomæne.dk";
            await Launcher.OpenAsync(new Uri($"mailto:{address}"));
        }
    }
}
