﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using MvvmHelpers;
using MvvmHelpers.Commands;
using SharedProject.Models;
using System.Net.Http;
using SharedProject;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Essentials;

namespace MobileApp.ViewModels.SettingsViewModels
{
    public class FeedbackViewModel : ViewModelBase
    {
        // Variables
        public FeedbackModel Feedback { get; set; }
        private FeedbackMood feedbackMood;
        private FeedBackType feedbackType;

        // Microbrain workaround
        public bool MoodFirst { get { return feedbackMood == FeedbackMood.sad; } set { if (value) feedbackMood = FeedbackMood.sad; } }
        public bool MoodSecond { get { return feedbackMood == FeedbackMood.neutral; } set { if (value) feedbackMood = FeedbackMood.neutral; } }
        public bool MoodThird { get { return feedbackMood == FeedbackMood.happy; } set { if (value) feedbackMood = FeedbackMood.happy; } }

        public bool TypeFirst { get { return feedbackType == FeedBackType.question; } set { if (value) feedbackType = FeedBackType.question; } }
        public bool TypeSecond { get { return feedbackType == FeedBackType.suggestion; } set { if (value) feedbackType = FeedBackType.suggestion; } }
        public bool TypeThird { get { return feedbackType == FeedBackType.error; } set { if (value) feedbackType = FeedBackType.error; } }
        public bool TypeFourth { get { return feedbackType == FeedBackType.other;  } set { if (value) feedbackType = FeedBackType.other;  } }

        // Commands
        public AsyncCommand SubmitCommand { get; }

        private readonly HttpClient _client;

        public FeedbackViewModel()
        {
            Feedback = new FeedbackModel();

            _client = HttpClientConstant.GetClient();

            SubmitCommand = new AsyncCommand(Submit);
        }

        async Task Submit()
        {
            IsBusy = true;

            Feedback.Mood = feedbackMood;
            Feedback.Type = feedbackType;

            string baseAddress = DeviceInfo.Platform == DevicePlatform.Android ? "https://10.0.2.2:7257" : "https://localhost:7257";
            var uri = new Uri(string.Format(baseAddress + "/api/Feedback/", string.Empty));

            StringContent content = new StringContent(JsonConvert.SerializeObject(Feedback), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await _client.PostAsync(uri, content);

            if (response.IsSuccessStatusCode)
            {
                Feedback = new FeedbackModel();
                await Application.Current.MainPage.DisplayAlert("Tak for din feedback!", "Vi vil læse den og handle snarest muligt.", "OK");
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Der skete en fejl!", "Prøv igen senere.", "OK");
            }

            OnPropertyChanged("Feedback");

            IsBusy = false;
        }
    }
}
