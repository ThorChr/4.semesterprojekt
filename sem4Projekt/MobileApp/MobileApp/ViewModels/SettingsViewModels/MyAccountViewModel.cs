﻿using MobileApp.Services;
using MvvmHelpers.Commands;
using SharedProject.Models.Auth;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobileApp.ViewModels.SettingsViewModels
{
    public class MyAccountViewModel : ViewModelBase
    {
        private readonly IUserService _userService;

        public UserDetailsModel UserDetails { get; set; }

        public AsyncCommand OnAppearCommand { get; set; }
        public AsyncCommand SaveChangesButtonPressed { get; set; }

        public MyAccountViewModel()
        {
            Title = "Min Konto";

            _userService = DependencyService.Get<IUserService>();

            // Commands.
            OnAppearCommand = new AsyncCommand(OnAppearAsync);
            SaveChangesButtonPressed = new AsyncCommand(SaveChangesAsync);
        }

        private async Task OnAppearAsync()
        {
            IsBusy = true;

            var userDetails = await _userService.GetUserDetails();
            
            if(userDetails is null)
            {
                // yeet an error
                await Application.Current.MainPage.DisplayAlert("Error", "Failed to fetch information about user. Sorry.", "OK");
            }

            // Update property.
            UserDetails = userDetails;
            OnPropertyChanged("UserDetails");

            IsBusy = false;
        }

        /// <summary>
        /// Saves whatever changes have been made to the UserDetails object.
        /// </summary>
        /// <returns></returns>
        private async Task SaveChangesAsync()
        {
            var result = await _userService.SaveUserDetails(UserDetails);
            
            if(!result.Success)
            {
                // yeet an error
                await Application.Current.MainPage.DisplayAlert("Error", "Failed to save changes. Sorry.", "OK");
                return;
            }

            // Update property.
            UserDetails = result.EditUser;
            OnPropertyChanged("UserDetails");

            await Application.Current.MainPage.DisplayAlert("Success", "Changes saved.", "OK"); 
        }
    }
}
