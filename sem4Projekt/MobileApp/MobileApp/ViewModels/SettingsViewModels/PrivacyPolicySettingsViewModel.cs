﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileApp.ViewModels.SettingsViewModels
{
    public class PrivacyPolicySettingsViewModel : ViewModelBase
    {
        public string Logo { get; set; }
        public PrivacyPolicySettingsViewModel()
        {
            Title = "Persondatapolitik";
            Logo = "https://avatars.akamai.steamstatic.com/5beb3b73d605c13383039d9fb4d228dd7637c319_full.jpg";
        }
    }
}
