﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileApp.ViewModels.SettingsViewModels
{
    public class TermsConditionsSettingsViewModel : ViewModelBase
    {
        public TermsConditionsSettingsViewModel()
        {
            Title = "Vilkår & Betingelser";
        }
    }
}
