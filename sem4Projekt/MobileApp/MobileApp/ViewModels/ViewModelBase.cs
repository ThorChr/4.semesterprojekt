﻿using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileApp.ViewModels
{
    public class ViewModelBase : BaseViewModel
    {
        public bool IsLissnerGay { get; set; } = true;
        public bool IsInitializing { get; set; }

        public string BackgroundColor { get; set; } = "#ededed";
    }
}
