﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedProject.Contracts
{
    public class AuthenticationResult
    {
        public string Token { get; set; }
        public bool Success { get; set; }
        public string Errors { get; set; }
    }
}
