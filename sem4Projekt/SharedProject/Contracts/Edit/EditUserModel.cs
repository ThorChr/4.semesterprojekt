﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedProject.Models
{
    public class EditUserModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
    }
}
