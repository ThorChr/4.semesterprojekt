﻿using SharedProject.Models;
using SharedProject.Models.Auth;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharedProject.Contracts.Edit
{
    public class EditUserResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public UserDetailsModel EditUser { get; set; }
    }
}
