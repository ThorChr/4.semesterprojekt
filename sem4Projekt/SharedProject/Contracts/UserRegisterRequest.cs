﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedProject.Contracts
{
    public class UserRegisterRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordVerify { get; set; }
    }
}
