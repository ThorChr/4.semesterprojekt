﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace SharedProject
{
    public class HttpClientConstant
    {
        public static HttpClient GetClient()
        {
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            // Pass the handler to httpclient(from you are calling api)
            HttpClient client = new HttpClient(clientHandler);

            return client;

            #region Old Code
            //_client = new HttpClient();
            //HttpClientHandler clientHandler = new HttpClientHandler();
            //clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            // Pass the handler to httpclient(from you are calling api)
            //_client = new HttpClient(clientHandler);
            #endregion
        }
    }
}
