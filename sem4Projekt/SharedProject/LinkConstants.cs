﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedProject
{
    public class LinkConstants
    {
        public const string Api = "https://localhost:7257/api/";
        public const string Partners = Api + "Partner/";
        public const string PartnersGet = Api + "Partner/Get/";
    }
}
