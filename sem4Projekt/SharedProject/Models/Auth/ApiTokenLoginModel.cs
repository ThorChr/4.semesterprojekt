﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedProject.Models
{
    public class ApiTokenLoginModel
    {
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
        public string Username { get; set; }
    }
}
