﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedProject.Models
{
    public class Constants
    {
        public const string XAccessToken = "X-Access-Token";
        public const string XAccessTokenExpiration = "X-Access-Token-Expiration";

    }

}
