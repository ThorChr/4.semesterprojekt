﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedProject.Models
{
    public class ResponseModel
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }

}
