﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedProject.Models.Auth
{
    public class UserDetailsModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
