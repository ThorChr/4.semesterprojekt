﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedProject.Models
{
    public class FeedbackModel
    {
        public int Id { get; set; }
        public FeedbackMood Mood { get; set; }
        public FeedBackType Type { get; set; }
        public string Content { get; set; }
    }

    public enum FeedbackMood
    {
        sad,
        neutral,
        happy
    }
    public enum FeedBackType
    {
        question,
        suggestion,
        error,
        other
    }
}
