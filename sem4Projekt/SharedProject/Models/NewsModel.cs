﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SharedProject.Models
{
    public class NewsModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; } = String.Empty;
        public string Image { get; set; } = String.Empty;

        [NotMapped]
        public string ShortDescription => Description.Length > 32 ? Description.Substring(0, 32) + "..." : Description;
    }
}
