﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedProject.Models
{
    public class PartnerModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Logo { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public ICollection<NewsModel> News { get; set; }


        public PartnerModel()
        {
            this.News = new HashSet<NewsModel>();
        }
    }
}
