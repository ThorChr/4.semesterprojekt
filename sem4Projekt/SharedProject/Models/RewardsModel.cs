﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedProject.Models
{
    public class RewardsModel
    {
        public int Id { get; set; }
        public string PartnerName { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
        public DateTime ExpirationDate { get; set; }
        public bool HasBeenUsed { get; set; } = false;
    }
}
