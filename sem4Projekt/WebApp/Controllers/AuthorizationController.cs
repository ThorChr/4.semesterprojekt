﻿using Microsoft.AspNetCore.Mvc;
using SharedProject;
using SharedProject.Models;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Net.Http.Headers;
using System.Text;
using WebApp.Helpers;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

namespace WebApp.Controllers
{
    [Route("Auth")]
    public class AuthorizationController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public AuthorizationController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        [Route("Login")]
        public IActionResult Login()
        {
            var model = new LoginModel();
            return View(model);
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(LoginModel model)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, LinkConstants.Api + "Authentication/Login");

            request.Content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");

            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.SendAsync(request);

            if(!response.IsSuccessStatusCode)
                return RedirectToAction(nameof(Login));

            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            var result = JsonSerializer.Deserialize<ApiTokenLoginModel>(json, options);

            // Some weird error happened.
            if(result == null)
                return RedirectToAction(nameof(Login));

            // Save token to cookie.
            CookieHelper.SetCookie(HttpContext, result);

            return RedirectToAction("Index", "Home");
        }

        [Route("Register")]
        public IActionResult Register()
        {
            var model = new RegisterModel();
            return View(model);
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, LinkConstants.Api + "Authentication/Register");

            request.Content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");

            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
                return RedirectToAction(nameof(Login));
            else
                return RedirectToAction(nameof(Register));
        }

        [HttpGet]
        [Route("Logout")]
        public async Task<IActionResult> Logout()
        {
            CookieHelper.RemoveCookie(HttpContext);
            return RedirectToAction("Index", "Home");
        }
    }
}
