﻿using Microsoft.AspNetCore.Mvc;
using SharedProject;
using SharedProject.Models;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Net.Http.Headers;
using System.Text;
using WebApp.Helpers;
using Microsoft.AspNetCore.Authorization;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    [Authorize]
    [Route("News")]
    public class NewsController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public NewsController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async  Task<IActionResult> Index()
        {
            NewsViewModel model = new NewsViewModel();
            model.FormModel = new NewsModel();

            // Make sure user has a partner before proceeding.
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, LinkConstants.Api + "User/GetPartner");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", CookieHelper.GetTokenFromCookie(HttpContext));

            var client = _httpClientFactory.CreateClient();
            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode) return RedirectToAction("Index","Settings");
            
            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            var partnerModelFromApi = JsonSerializer.Deserialize<PartnerModel>(json, options);
            
            if (partnerModelFromApi is null) return RedirectToAction("Index", "Settings");

            // Get news from specific partner.
            HttpRequestMessage requestNewsFromPartner = new HttpRequestMessage(HttpMethod.Get, LinkConstants.Api + "News/GetAllFromPartner/" + partnerModelFromApi.Id);
            requestNewsFromPartner.Headers.Authorization = new AuthenticationHeaderValue("Bearer", CookieHelper.GetTokenFromCookie(HttpContext));

            var client3 = _httpClientFactory.CreateClient();
            var responseNewsFromPartner = await client3.SendAsync(requestNewsFromPartner);

            if (!responseNewsFromPartner.IsSuccessStatusCode) return View(model);

            var newsJson = await responseNewsFromPartner.Content.ReadAsStringAsync();

            // If the news is null from the API.
            if (newsJson == "") return View(model);

            var newsResult = JsonSerializer.Deserialize<List<NewsModel>>(newsJson, options);
            model.ListModel = newsResult;

            return View(model);
        }

        [HttpPost]
        [Route("CreateNews")]
        public async Task<IActionResult> CreateNews(NewsViewModel viewmodel)
        {
            var model = viewmodel.FormModel;
            model.Image = (model.Image is null) ? "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg" : model.Image;
            model.Link = (model.Link is null) ? " " : model.Link;

            // Create http client and handle relevant stuff.
            HttpClient client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // Create the request.
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, LinkConstants.Api + "News");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", CookieHelper.GetTokenFromCookie(HttpContext));
            request.Content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");

            // Send the request and handle the response.
            var response = client.Send(request);
            if (!response.IsSuccessStatusCode) return RedirectToAction(nameof(Index));

            return RedirectToAction(nameof(Index));
        }

        [Route("{id:int}")]            
        public async Task<IActionResult> News(int id)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, LinkConstants.Api + "News/" + id);

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", CookieHelper.GetTokenFromCookie(HttpContext));

            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.SendAsync(request);

            if(!response.IsSuccessStatusCode)
                return RedirectToAction(nameof(Index));

            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            var result = JsonSerializer.Deserialize<NewsModel>(json, options);

            return View(result);
        }

        [HttpPost]
        [Route("UpdateNews")]
        public async Task<IActionResult> UpdateNews(NewsModel model)
        {
            model.Image = (model.Image is null) ? "https://avatars.cloudflare.steamstatic.com/f3059f9e1f1482db055fee2a3dced7f7b3e103f0_full.jpg" : model.Image;
            model.Link = (model.Link is null) ? " " : model.Link;

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, LinkConstants.Api + "News/");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", CookieHelper.GetTokenFromCookie(HttpContext));
            request.Content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");

            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
                return RedirectToAction(nameof(Index));

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Route("DeleteNews")]
        public async Task<IActionResult> DeleteNews(int id)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, LinkConstants.Api + "News/" + id);

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", CookieHelper.GetTokenFromCookie(HttpContext));

            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
                return RedirectToAction(nameof(Index));

            return RedirectToAction(nameof(Index));
        }


    }
}
