﻿using Microsoft.AspNetCore.Mvc;
using SharedProject;
using SharedProject.Models;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Net.Http.Headers;
using System.Text;
using WebApp.Helpers;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    [Authorize]
    public class SettingsController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public SettingsController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        [Route("Partner")]
        public async Task<IActionResult> Partner()
        {
            PartnerModel model = new PartnerModel();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, LinkConstants.Api + "Partner/GetAssociatedPartner");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", CookieHelper.GetTokenFromCookie(HttpContext));

            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.SendAsync(request);

            if(!response.IsSuccessStatusCode)
                return View(model);

            var json = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            var result = JsonSerializer.Deserialize<PartnerModel>(json, options);

            return View(result);
        }

        [HttpPost]
        [Route("CreatePartner")]
        public async Task<IActionResult> CreatePartner(PartnerModel model)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, LinkConstants.Api + "Partner/");

            request.Content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", CookieHelper.GetTokenFromCookie(HttpContext));

            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
                return RedirectToAction(nameof(Partner));

            return RedirectToAction(nameof(Partner));
        }

        [HttpPost]
        [Route("UpdatePartner")]
        public async Task<IActionResult> UpdatePartner(PartnerModel model)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, LinkConstants.Api + "Partner/" + model.Id);

            request.Content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", CookieHelper.GetTokenFromCookie(HttpContext));

            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
                return RedirectToAction(nameof(Partner));

            return RedirectToAction(nameof(Partner));
        }

        [HttpPost]
        [Route("DeletePartner")]
        public async Task<IActionResult> DeletePartner(int id)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, LinkConstants.Partners + id);

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", CookieHelper.GetTokenFromCookie(HttpContext));

            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
                return RedirectToAction(nameof(Partner));

            return RedirectToAction(nameof(Partner));
        }

        [Route("User")]
        public IActionResult User()
        {
            return View();
        }
    }
}
