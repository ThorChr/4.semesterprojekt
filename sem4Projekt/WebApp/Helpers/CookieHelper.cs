﻿using SharedProject.Models;

namespace WebApp.Helpers
{
    public static class CookieHelper
    {
        // The default name for our cookie.
        private const string _cookieName = Constants.XAccessToken;

        /// <summary>
        /// Method used for storing a jwt token in a Cookie.
        /// </summary>
        /// <param name="response">The response object used by the controller trying to save the Cookie.</param>
        /// <param name="tokenModel">The token object retrieved from the API.</param>
        /// <param name="cookieName">If custom name is needed, specify here.</param>
        /// <returns></returns>
        public static bool SetCookie(HttpContext context, ApiTokenLoginModel tokenModel, string cookieName = _cookieName)
        {
            try
            {
                // Let's quickly check if cookie already exists - if do, then delete it.
                if (CookieAlreadyExists(context, cookieName))
                    RemoveCookie(context);

                // Set the new cookie.
                context.Response.Cookies.Append(cookieName, tokenModel.Token, new CookieOptions
                {
                    HttpOnly = true,
                    SameSite = SameSiteMode.Strict,
                    Expires = tokenModel.Expiration
                });

                return true;
            } catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a cookie's value.
        /// </summary>
        /// <param name="request">The request object.</param>
        /// <param name="cookieName">If targeting a specific cookie, use this to specify, otherwise will use default name.</param>
        /// <returns></returns>
        public static string GetTokenFromCookie(HttpContext context, string cookieName = _cookieName)
        {
            var val = context.Request.Cookies[_cookieName];

            if (val == null)
                return "error";
            else
                return val;
        }

        /// <summary>
        /// Used for checking if a specific cookie already exists.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cookieName"></param>
        /// <returns></returns>
        public static bool CookieAlreadyExists(HttpContext context, string cookieName = _cookieName)
        {
            if (context.Request.Cookies[_cookieName] == null)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Used for removing a cookie. Default is the jwt cookie.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cookieName"></param>
        public static void RemoveCookie(HttpContext context, string cookieName = _cookieName)
        {
            if(CookieAlreadyExists(context, cookieName))
                context.Response.Cookies.Delete(cookieName);
        }
    }
}
