﻿using System;
using System.Collections.Generic;
using System.Text;
using SharedProject.Models;

namespace WebApp.ViewModels
{
    public class NewsViewModel
    {
        public NewsModel FormModel { get; set; }
        public List<NewsModel> ListModel { get; set; }
    }
}
